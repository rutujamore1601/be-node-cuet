
const router = require("express").Router();
const mockTestController = require('./mockTest.controller');

// Create Operation - Create mockTest
router.post('/createMockTest', mockTestController.createMockTest);

// Read Operation - Get all mockTest
router.get('/getAllMockTest', mockTestController.getAllMockTest);

// Read Operation - Get all mockTest by Id 
router.get("/getAllMockTestById/:id", mockTestController.getAllMockTestById);

// Read Operation - Get a single mockTest by Id
router.get('/getMockTestById/:id', mockTestController.getMockTestById);

// router.get('/getMockTestBySubjectId/:classId', mockTestController.getMockTestBySubjectId);
router.get('/getMockTestBySubjectId/:subjectId/:userId', mockTestController.getMockTestBySubjectId);

// Update Operation - Update mockTest
router.put('/updateMockTest/:id', mockTestController.updateMockTest);

// Delete Operation - Delete mockTest
router.delete('/deleteMockTest/:id', mockTestController.deleteMockTest);

module.exports = router;
