const mockTestModel = require("./mockTest.model");
const mockTestResultModel = require("../submitmockTest/submitmockTest.model");
const mockTestQuestionModel = require("./../mockTestQuestions/mockTestQuestion.model");
const SubjectModel = require("../subject/subject.model");

// Create Operation - Create mockTest
const createMockTest = (req, res) => {
  if (!req.body) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }

  const mockTest = new mockTestModel(req.body);
  mockTest
    .save()
    .then((data) => {
      res.status(200).send({
        data: data,
        dataCount: data.length,
        message: "mockTest created successfully!",
        success: true,
        statusCode: 200,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the mockTest.",
      });
    });
};

// Read Operation - Get all mockTest
// const getAllMockTest = (req, res) => {
//   mockTestModel.find()
//     .then((data) => {
//       if (data.length > 0) {
//         res.status(200).send({
//           data: data,
//           dataCount: data.length,
//           message: "mockTest fetch successfully!",
//           success: true,
//           statusCode: 200,
//         });
//       } else {
//         res.status(200).send({
//           data: [],
//           dataCount: data.length,
//           message: "mockTest not found!",
//           success: false,
//           statusCode: 200,
//         });
//       }
//     })
//     .catch((err) => {
//       res.status(500).send({
//         message:
//           err.message || "Some error occurred while Retrieve the mockTest.",
//       });
//     });
// };

const getAllMockTest = async (req, res) => {
  try {
    const mockTests = await mockTestModel.find();

    if (mockTests.length === 0) {
      res.status(404).json({ error: "Mock Test data not found." });
      return;
    }

    const result = await Promise.all(
      mockTests.map(async (test) => {
        const subjectDetails = await SubjectModel.findById(test.subjectId);
        console.log("subjectDetails", subjectDetails);

        if (!subjectDetails) {
          return {
            error: `Subject details not found for subjectId: ${test.subjectId}`,
          };
        }

        return {
          subject: {
            subjectId: subjectDetails._id,
            subjectName: subjectDetails.subjectName,
            subjectImg: subjectDetails.subjectImg,
          },
          testName: test.testName,
          testTimeDuration: test.testTimeDuration,
        };
      })
    );

    res.status(200).json(result);
  } catch (error) {
    console.error("Error:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

// Read Operation - Get all mockTest by Id
const getAllMockTestById = (req, res) => {
  const id = req.params.id;
  const condition = { _id: id };

  mockTestModel
    .find(condition)
    .then((data) => {
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "mockTest fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: [],
          dataCount: data.length,
          message: "mockTest not found!",
          success: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while Retrieve the mockTest.",
      });
    });
};

// Read Operation - Get a single mockTest by Id
const getMockTestById = (req, res) => {
  const id = req.params.id;
  mockTestModel
    .findById(id)
    .then((data) => {
      if (data) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "mockTest fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: {},
          dataCount: 0,
          message: "mockTest not found with ID=" + id,
          status: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while Retrieve the mockTest.",
      });
    });
};

const getMockTestBySubjectId = async (req, res) => {
  try {
    const subjectId = req.params.subjectId;
    const userId = req.params.userId;

    const data = await mockTestModel.find({ subjectId }).lean();
    console.log("data", data);

    const results = await Promise.all(
      data.map(async (element) => {
        const resultInfo = await mockTestResultModel
          .findOne({
            $and: [{ testId: element._id }, { userId: userId }],
          })
          .sort({ createdAt: -1 });

        if (resultInfo) {
          element.isAttempted = true;
          element.resultDetails = resultInfo;
        } else {
          element.isAttempted = false;
          element.resultDetails = {};
          element.resultDetails["totalQuestion"] = await mockTestQuestionModel
            .countDocuments({ testId: element._id })
            .exec();
        }

        return element;
      })
    );

    if (data.length > 0) {
      res.status(200).send({
        data: results,
        dataCount: data.length,
        message: "Mock test fetch successfully!",
        success: true,
        statusCode: 200,
      });
    } else {
      res.status(200).send({
        data: {},
        dataCount: 0,
        message: `Mock test not found with subjectId=${subjectId}`,
        status: false,
        statusCode: 200,
      });
    }
  } catch (err) {
    res.status(500).send({
      message:
        err.message || "Some error occurred while retrieving the mock test.",
    });
  }
};

// Update Operation - Update mockTest
const updateMockTest = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  mockTestModel
    .findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "mockTest was updated successfully.",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot update mockTest with order ID=" +
            id +
            ". Maybe mockTest was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating mockTest with ID=" + id,
      });
    });
};

// Delete Operation - Delete mockTest
const deleteMockTest = (req, res) => {
  const id = req.params.id;

  mockTestModel
    .findByIdAndDelete(id)
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "mockTest was deleted successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot delete mockTest with ID=" +
            id +
            ". Maybe mockTest was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Tutorial.",
      });
    });
};

module.exports = {
  createMockTest,
  getAllMockTest,
  getAllMockTestById,
  getMockTestById,
  updateMockTest,
  deleteMockTest,
  getMockTestBySubjectId,
};
