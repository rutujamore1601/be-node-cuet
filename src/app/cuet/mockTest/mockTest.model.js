
const mongoose = require('mongoose');

const mockTestSchema = new mongoose.Schema({
  subjectId: { 
    type: mongoose.Schema.Types.ObjectId,
      ref: "subject",
      required: false,
   },
  testName: { type: String, required: false },
  testTimeDuration: { type: String, required: false },
}, { timestamps: true });

module.exports = mongoose.model('mockTest', mockTestSchema);
