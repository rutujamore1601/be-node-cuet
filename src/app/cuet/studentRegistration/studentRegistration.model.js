const mongoose = require("mongoose");

const studentRegistrationSchema = new mongoose.Schema(
  {
    streamId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "streams",
      required: false,
    },
    fullName: { type: String, required: true },
    mobileNo: { type: String, required: true, unique: true },
    email: { type: String, required: false },
    otp: { type: String, required: false },
  },
  { timestamps: true }
);

module.exports = mongoose.model(
  "studentRegistration",
  studentRegistrationSchema
);
