const studentRegistrationModel = require("./studentRegistration.model");
const commonUtils = require("../../../shared/utils/common.util");
const streamModel = require("../stream/stream.model");

const sendOtp = (req, res) => {};

const generateOTP = (mobileNo) => {
  // Generate OTP logic here (for demo purposes, it always returns '1234')
  return "1234";
};

// student createStudentRegistration
// Create Operation - Create studentRegistration

const createStudentRegistration = async (req, res) => {
  try {
    const { streamId, email, fullName, mobileNo } = req.body;

    if (!streamId || !email || !fullName || !mobileNo) {
      res.status(400).json("Please add all fields");
      return;
    }
    const stream = await streamModel.findById(streamId);
    if (!stream) {
      res.status(404).json(`Stream of Id ${streamId} not found`);
      return;
    }

    const registerUser = await studentRegistrationModel.create({ ...req.body });
    if (!registerUser) {
      res.status(400).json("failed to register user..");
    }

    res.status(201).json("User created successfully..");
  } catch (error) {
    console.log("error", error);
    if (error.code === 11000 && error.keyPattern && error.keyPattern.mobileNo) {
      res
        .status(400)
        .json(
          "mobile number already exists. Please use a different mobile number."
        );
    } else {
      res.status(500).json("Internal server error..");
    }
  }
};

// Submit OTP for verification
const submitOtp = (req, res) => {
  const userData = req.body;

  if (!req.body) {
    res.status(400).send({ message: "Mobile number and OTP are required!" });
    return;
  }

  // Get the generated OTP for the provided mobile number
  const generatedOTP = generateOTP(userData.mobileNo);

  if (userData.otp === generatedOTP) {
    // OTP is correct, generate a token
    const token = commonUtils.generateToken({ mobileNo: userData.mobileNo });

    // Fetch user data from the database using mobileNo
    studentRegistrationModel.findOne(
      { mobileNo: userData.mobileNo },
      (err, user) => {
        console.log("user", user);
        if (err || !user) {
          res.status(500).send({
            message: "Error fetching user data!",
            authentication: false,
            success: false,
            statusCode: 500,
          });
          return;
        }

        // User data found, send it in the response along with the token
        res.status(200).send({
          token: token,
          userData: user,
          message: "OTP verification successful. Login successful!",
          authentication: true,
          success: true,
          statusCode: 200,
        });
      }
    );
  } else {
    // Invalid OTP
    res.status(401).send({
      message: "Invalid OTP! Please try again.",
      authentication: false,
      success: false,
      statusCode: 401,
    });
  }
};

// Read Operation - Get all studentRegistration
const getAllStudentRegistration = async (req, res) => {
  try {
    const page = parseInt(req.body.page) || 1;
    const limit = parseInt(req.body.limit) || 25;
    const query = buildQuery(req.body);

    const [data, totalData] = await Promise.all([
      studentRegistrationModel
        .find(query)
        .lean()
        .skip((page - 1) * limit)
        .limit(limit),
      studentRegistrationModel.countDocuments(query),
    ]);

    if (data.length > 0) {
      res.status(200).send({
        data: data,
        message: "studentRegistration Data fetched successfully!",
        success: true,
        statusCode: 200,
        totalData: totalData,
      });
    } else {
      res.status(200).send({
        data: [],
        message: "studentRegistration Data not found!",
        success: false,
        statusCode: 200,
        totalData: 0,
      });
    }
  } catch (err) {
    res.status(500).send({
      message:
        err.message ||
        "Some error occurred while retrieving the studentRegistration Data.",
    });
  }
};

// Read Operation - Get all studentRegistration by Id
const getAllStudentRegistrationById = (req, res) => {
  const id = req.params.id;
  const condition = { _id: id };

  studentRegistrationModel
    .find(condition)
    .then((data) => {
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "studentRegistration fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: [],
          dataCount: data.length,
          message: "studentRegistration not found!",
          success: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while Retrieve the studentRegistration.",
      });
    });
};

// Read Operation - Get a single studentRegistration by Id
const getStudentRegistrationById = (req, res) => {
  const id = req.params.id;
  studentRegistrationModel
    .findById(id)
    .then((data) => {
      if (data) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "studentRegistration fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: {},
          dataCount: 0,
          message: "studentRegistration not found with ID=" + id,
          status: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while Retrieve the studentRegistration.",
      });
    });
};

const getStudentRegistrationByUserType = (req, res) => {
  const userType = req.params.userType;

  studentRegistrationModel
    .find({ userType: userType })
    .then((data) => {
      if (data) {
        res.status(200).send({
          data: data,
          dataCount: 1,
          message: "Student registration fetched successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          data: {},
          dataCount: 0,
          message: "Student registration not found with userType=" + userType,
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          "Some error occurred while retrieving the student registration.",
        success: false,
        statusCode: 500,
      });
    });
};

// Update Operation - Update studentRegistration
const updateStudentRegistration = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  // const { password, confirmpassword } = req.body;
  // console.log('req.body', req.body);

  // const coordinatorCode = req.body.coordinatorCode;

  // registrationModel.findOne({ coordinatorCode: coordinatorCode })
  //   .then((cordinator) => {

  //     if (password !== confirmpassword) {
  //       res.status(400).send({ message: "Password and confirm password do not match!" });
  //       return;
  //     }

  //     cordinator ? (req.body.coordinatorName = cordinator.name) : '';

  studentRegistrationModel
    .findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (data) {
        res.status(200).send({
          data: data,
          message: "studentRegistration was updated successfully.",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot update studentRegistration with order ID=" +
            id +
            ". Maybe studentRegistration was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating studentRegistration with ID=" + id,
      });
    });
  // })
};

// Delete Operation - Delete studentRegistration
const deleteStudentRegistration = (req, res) => {
  const id = req.params.id;

  studentRegistrationModel
    .findByIdAndDelete(id)
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "studentRegistration was deleted successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot delete studentRegistration with ID=" +
            id +
            ". Maybe studentRegistration was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Tutorial.",
      });
    });
};

// create Operation - login
// const login = async ((req, res) => {
//   const { mobileNo } = req.body;
//   // console.log("userId", userId);

//   if (mobileNo) {
//     res.status(400).send({ message: "userId and password are required." });
//     return;
//   }

//   studentRegistrationModel.findOne({ userId, password }, (err, user) => {
//     if (err) {
//       res.status(500).send({ message: "Internal server error." });
//       return;
//     }

//     if (user) {
//       res.status(200).send({
//         data: user,
//         message: "Login successful!",
//         success: true,
//         statusCode: 200,
//       });
//     } else {
//       res.status(400).send({
//         data: user,
//         message: "Login failed. Invalid userId or password.",
//         success: false,
//         statusCode: 400,
//       });
//     }
//   });
// });

const login = async (req, res) => {
  try {
    const { mobileNo } = req.body;
    if (!mobileNo) {
      res.status(400).json({ message: "Mobile No is required." });
      return;
    }

    const generateRandomOTP = () => {
      const min = 1000; // Minimum 4-digit number
      const max = 9999; // Maximum 4-digit number
      return Math.floor(Math.random() * (max - min + 1)) + min;
    };

    const otp = generateRandomOTP();
    const userLogin = await studentRegistrationModel.findOne({
      mobileNo: mobileNo,
    });
    if (!userLogin) {
      res.status(404).json({ message: "User not found." });
      return;
    }

    const otpUpdate = await studentRegistrationModel.updateOne(
      { mobileNo: mobileNo },
      { otp: otp }
    );

    if (!otpUpdate) {
      res.status(500).json({ message: "Failed to update OTP." });
      return;
    }

    res.status(200).json({ message: "OTP sent successfully.", otp: otp });
  } catch (error) {
    console.log("error", error);
    res.status(500).send({ message: "Internal server error." });
  }
};

const forgetPassword = async (req, res) => {
  try {
    const { mobileNo } = req.body;
    console.log("mobileNo", mobileNo);

    if (!mobileNo) {
      res.status(400).json({
        status: 400,
        error: "400",
        message: "Please Enter Your Mobile Number",
      });
      return;
    }

    const studentVerification = await studentRegistrationModel.findOne({
      mobileNo: mobileNo,
    });
    console.log("studentVerification", studentVerification);
    if (!studentVerification) {
      res.status(404).json({
        status: 404,
        error: "404",
        message: `User with Mobile No ${mobileNo} is not found`,
      });
      return;
    }

    const generateRandomOTP = () => {
      const min = 1000; // Minimum 4-digit number
      const max = 9999; // Maximum 4-digit number
      return Math.floor(Math.random() * (max - min + 1)) + min;
    };

    const otp = generateRandomOTP();
    const updateOtp = await studentRegistrationModel.findByIdAndUpdate(
      studentVerification._id,
      { otp: otp },
      { new: true }
    );

    if (!updateOtp) {
      res
        .status(400)
        .json({ status: 400, error: "400", message: "Failed To Send OTP" });
      return;
    }

    const result = {
      mobileNo: updateOtp.mobileNo,
      NewOTP: updateOtp.otp,
    };

    res.status(200).json({ result });
  } catch (error) {
    console.log("error", error);
    res
      .status(500)
      .json({ status: 500, error: "500", message: "Internal Server Error" });
  }
};

const otpVerify = async (req, res) => {
  try {
    const { mobileNo, newOtp } = req.body;

    const studentVerification = await studentRegistrationModel.findOne({
      mobileNo,
    });

    if (!studentVerification) {
      return res.status(404).json({
        status: 404,
        error: "404",
        message: "User not found with provided mobile number",
      });
    }

    if (studentVerification.otp == newOtp) {
      return res.status(200).json({
        status: 200,
        message: "OTP verification successful",
        mobileNo,
      });
    }

    return res.status(400).json({
      status: 400,
      error: "400",
      message: "Failed To Verify OTP",
    });
  } catch (error) {
    console.error("Error:", error);
    res.status(500).json({
      status: 500,
      error: "500",
      message: "Internal Server Error",
    });
  }
};

const resetPassword = async (req, res) => {
  try {
    const { mobileNo, password, confirmPassword } = req.body;

    if (!mobileNo || !password || !confirmPassword) {
      return res.status(400).json({
        status: 400,
        error: "400",
        message: "Missing required parameters",
      });
    }

    if (password !== confirmPassword) {
      return res.status(400).json({
        status: 400,
        error: "400",
        message: "Password and Confirm Password should be the same",
      });
    }

    const passwordReset = await studentRegistrationModel.findOneAndUpdate(
      { mobileNo: mobileNo },
      { password: password, confirmpassword: confirmPassword },
      { new: true }
    );

    if (!passwordReset) {
      return res.status(400).json({
        status: 400,
        error: "400",
        message: "Failed to reset password",
      });
    }

    return res.status(200).json({ message: "Password updated successfully" });
  } catch (error) {
    console.error("Error:", error);
    return res
      .status(500)
      .json({ status: 500, error: "500", message: "Internal Server Error" });
  }
};

module.exports = {
  resetPassword,
  createStudentRegistration,
  getAllStudentRegistration,
  getAllStudentRegistrationById,
  getStudentRegistrationById,
  updateStudentRegistration,
  deleteStudentRegistration,
  login,
  submitOtp,
  forgetPassword,
  otpVerify,
  resetPassword,
  getStudentRegistrationByUserType,
};

// Function to build the query dynamically
const buildQuery = (body) => {
  const filters = {
    firstName: "firstName",
    class: "class",
    medium: "medium",
  };

  const query = {};

  for (const key in filters) {
    if (body[filters[key]] && body[filters[key]].length > 0) {
      if (key === "firstName") {
        query[key] = { $regex: body[filters[key]], $options: "i" };
      } else if (key === "class") {
        query[key] = { $regex: body[filters[key]], $options: "i" };
      } else if (key === "medium") {
        query[key] = { $regex: body[filters[key]], $options: "i" };
      } else {
        query[key] = { $in: body[filters[key]] };
      }
    }
  }
  return query;
};
