
const router = require("express").Router();
const streamController = require('./stream.controller');

// Create Operation - Create stream
router.post('/createstream', streamController.createstream);

// Read Operation - Get all stream
router.get('/getAllstream', streamController.getAllstream);

// Read Operation - Get all stream by Id 
router.get("/getAllstreamById/:id", streamController.getAllstreamById);

// Read Operation - Get a single stream by Id
router.get('/getstreamById/:id', streamController.getstreamById);

router.get('/getstreamBySubject/:subject', streamController.getstreamBySubject);

// Update Operation - Update stream
router.put('/updatestream/:id', streamController.updatestream);

// Delete Operation - Delete stream
router.delete('/deletestream/:id', streamController.deletestream);

module.exports = router;
