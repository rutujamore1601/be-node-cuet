
const streamModel = require('./stream.model');

  // Create Operation - Create stream
  const createstream = (req, res) => {
    if (!req.body) {
      res.status(400).send({ message: "Content can not be empty!" });
      return;
    }
  
    const stream = new streamModel(req.body);
    stream
      .save()
      .then((data) => {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "stream created successfully!",
          success: true,
          statusCode: 200,
        });
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the stream.",
        });
      });
  };

  // Read Operation - Get all stream
  const getAllstream = (req, res) => {
    streamModel.find()
      .then((data) => {
        if (data.length > 0) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "stream fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: [],
            dataCount: data.length,
            message: "stream not found!",
            success: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the stream.",
        });
      });
  };

  // Read Operation - Get all stream by Id 
  const getAllstreamById  = (req, res) => {
    const id = req.params.id;
    const condition = { _id: id};

    streamModel.find(condition)
      .then((data) => {
        if (data.length > 0) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "stream fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: [],
            dataCount: data.length,
            message: "stream not found!",
            success: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the stream.",
        });
      });
  };

  // Read Operation - Get a single stream by Id
  const getstreamById = (req, res) => {
    const id = req.params.id;
    streamModel.findById(id)
      .then((data) => {
        if (data) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "stream fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: {},
            dataCount: 0,
            message: 'stream not found with ID=' + id,
            status: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the stream.",
        });
      });
  };

   // Read Operation - Get a single subject by ClassId
   const getstreamBySubject = (req, res) => {
    const subject = req.params.subject;
    streamModel.findOne({ subject: subject })
      .then((data) => {
        if (data) {
          res.status(200).send({
            data: data,
            dataCount: 1,
            message: "stream fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: {},
            dataCount: 0,
            message: 'stream not found with ID=' + subject,
            status: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the stream.",
        });
      });
};

  // Update Operation - Update stream
 const updatestream = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  streamModel.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "stream was updated successfully.",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message: 'Cannot update stream with order ID=' + id + '. Maybe stream was not found!',
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating stream with ID=" + id,
      });
    });
};

// Delete Operation - Delete stream
  const deletestream = (req, res) => {
    const id = req.params.id;
  
    streamModel.findByIdAndDelete(id)
      .then((data) => {
        if (data) {
          res.status(200).send({
            message: "stream was deleted successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(404).send({
            message: 'Cannot delete stream with ID=' + id + '. Maybe stream was not found!',
            success: false,
            statusCode: 404,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Tutorial.",
        });
      });
  };

module.exports = {
  createstream,
  getAllstream,
  getAllstreamById,
  getstreamById,
  updatestream,
  deletestream,
  getstreamBySubject
};
