
const mongoose = require('mongoose');

const streamSchema = new mongoose.Schema({
  streamName: { type: String, required: false },
  streamImg: { type: String, required: false }
}, { timestamps: true });

module.exports = mongoose.model('stream', streamSchema);
