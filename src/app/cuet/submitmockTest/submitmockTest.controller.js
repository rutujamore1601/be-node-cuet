const submitmockTestModel = require("./submitmockTest.model");
const mockTestQuestionModel = require("./../mockTestQuestions/mockTestQuestion.model");

const submitMockTest = async (req, res) => {
  try {
    if (!req.body) {
      return res.status(400).json({ message: "Content can not be empty!" });
    }

    req.body.totalQuestion = await mockTestQuestionModel
      .countDocuments({ mockTestId: req.body.mockTestId })
      .exec();
    req.body.attemptedQuestion = 0;
    req.body.unAttemptedQuestion = 0;
    req.body.correctAnswer = 0;
    req.body.wrongAnswer = 0;
    req.body.negativeMarks = 0;
    req.body.correctQuestionNumbers = [];
    req.body.wrongQuestionNumbers = [];

    for (let i = 0; i < req.body.answerSheet.length; i++) {
      const element = req.body.answerSheet[i];
      req.body.attemptedQuestion = req.body.attemptedQuestion + 1;

      try {
        let mockQueDetail = await mockTestQuestionModel
          .findById(element.questionId)
          .select("optionList")
          .exec();
        const matchingOption = mockQueDetail?.optionList.find(
          (option) => option.optionId === element.answerId
        );
        element["isCorrect"] = matchingOption?.isCorrect ?? false;

        if (element.isCorrect) {
          req.body.correctAnswer = req.body.correctAnswer + 1;
          req.body.correctQuestionNumbers.push(i + 1);
        } else {
          req.body.wrongAnswer = req.body.wrongAnswer + 1;
          req.body.wrongQuestionNumbers.push(i + 1);
        }
      } catch (error) {
        element["isCorrect"] = false;
        req.body.wrongQuestionNumbers.push(i + 1);
      }
    }

    req.body.unAttemptedQuestion =
      req.body.totalQuestion - req.body.attemptedQuestion;
    req.body.totalMarks = req.body.totalQuestion * 2;
    req.body.negativeMarks = req.body.wrongAnswer * 0.5;

    // Calculate the marks with 2 marks per correct answer
    // 0.5 marks deduction for each wrong answer
    req.body.obtainedMarks =
      req.body.correctAnswer * 2 - req.body.wrongAnswer * 0.5;

    // Calculate the percentage
    // const percentageGot = (req.body.obtainedMarks / req.body.totalMarks) * 100;
    const percentageGot = Number(
      ((req.body.obtainedMarks / req.body.totalMarks) * 100).toFixed(2)
    );

    // Check if the percentage is greater than or equal to 40
    const passingCriteria = 40;

    req.body.testResult = percentageGot >= passingCriteria ? "Pass" : "Fail";
    req.body.obtainedPercentage = percentageGot;

    // Write logic here to save in DB and send the response back to the user
    const submitmockTest = new submitmockTestModel(req.body);
    submitmockTest
      .save()
      .then((data) => {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "submitmockTest created successfully!",
          success: true,
          statusCode: 200,
        });
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message ||
            "Some error occurred while creating the submitmockTest.",
        });
      });
  } catch (error) {
    console.error("Error:", error);
    res.status(500).json({ message: "Internal Server Error" });
  }
};

// Read Operation - Get a single submitmockTest by userId and mocktestId
const getsubmitMockTestByIds = (req, res) => {
  const id = req.params.mockTestId;
  const userId = req.params.userId;

  submitmockTestModel
    .find({ id, userId })
    .sort({ _id: -1 })
    .limit(1)
    .then((data) => {
      let isAttempted = false;

      if (data && data.length > 0) {
        isAttempted = true;
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "Latest submitmockTest data fetched successfully!",
          success: true,
          statusCode: 200,
          isAttempted: isAttempted,
        });
      } else {
        res.status(200).send({
          data: {},
          dataCount: 0,
          message: "submitmockTest not found with ID=" + id,
          status: false,
          statusCode: 200,
          isAttempted: isAttempted,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while retrieving the submitmockTest.",
      });
    });
};

const getAllsubmitmockTest = (req, res) => {
  submitmockTestModel
    .find()
    .populate({
      path: "userId",
    })
    .populate({
      path: "mockTestId",
    })
    .then((data) => {
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "submitmockTest fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: [],
          dataCount: data.length,
          message: "submitmockTest not found!",
          success: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while Retrieve the submitmockTest.",
      });
    });
};

// const getAllsubmitmockTest = async (req, res) => {
//   try {
//     const mockTest = await submitmockTestModel.find();
//     if (mockTest.length === 0) {
//       res.status(404).json("Data not found");
//       return;
//     }

//     res.status(200).json(mockTest);
//   } catch (error) {
//     console.log("error", error);
//     res.status(500).json("Internal Server Error");
//   }
// };

// Read Operation - Get all submitmockTest by Id
const getAllsubmitmockTestById = (req, res) => {
  const id = req.params.id;
  const condition = { _id: id };

  submitmockTestModel
    .find(condition)
    .then((data) => {
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "submitmockTest fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: [],
          dataCount: data.length,
          message: "submitmockTest not found!",
          success: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while Retrieve the submitmockTest.",
      });
    });
};

// Read Operation - Get a single submitmockTest by Id
const getsubmitmockTestById = (req, res) => {
  const id = req.params.id;
  submitmockTestModel
    .findById(id)
    .then((data) => {
      if (data) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "submitmockTest fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: {},
          dataCount: 0,
          message: "submitmockTest not found with ID=" + id,
          status: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while Retrieve the submitmockTest.",
      });
    });
};

// Update Operation - Update submitmockTest
const updatesubmitmockTest = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  submitmockTestModel
    .findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "submitmockTest was updated successfully.",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot update submitmockTest with order ID=" +
            id +
            ". Maybe submitmockTest was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating submitmockTest with ID=" + id,
      });
    });
};

// Delete Operation - Delete submitmockTest
const deletesubmitmockTest = (req, res) => {
  const id = req.params.id;

  submitmockTestModel
    .findByIdAndDelete(id)
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "submitmockTest was deleted successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot delete submitmockTest with ID=" +
            id +
            ". Maybe submitmockTest was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Tutorial.",
      });
    });
};

module.exports = {
  submitMockTest,
  getAllsubmitmockTest,
  getAllsubmitmockTestById,
  getsubmitmockTestById,
  updatesubmitmockTest,
  deletesubmitmockTest,
  getsubmitMockTestByIds,
};
