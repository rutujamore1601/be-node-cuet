
const router = require("express").Router();
const submitmockTestController = require('./submitmockTest.controller');

// Create Operation - Create submitmockTest
router.post('/submitMockTest', submitmockTestController.submitMockTest);

// Read Operation - Get all submitmockTest
router.get('/getAllsubmitmockTest', submitmockTestController.getAllsubmitmockTest);

// Read Operation - Get all submitmockTest by Id 
router.get("/getAllsubmitmockTestById/:id", submitmockTestController.getAllsubmitmockTestById);

// Read Operation - Get a single submitmockTest by Id
router.get('/getsubmitmockTestById/:id', submitmockTestController.getsubmitmockTestById);

router.get('/getsubmitMockTestByIds/:id/:userId', submitmockTestController.getsubmitMockTestByIds);

// Update Operation - Update submitmockTest
router.put('/updatesubmitmockTest/:id', submitmockTestController.updatesubmitmockTest);

// Delete Operation - Delete submitmockTest
router.delete('/deletesubmitmockTest/:id', submitmockTestController.deletesubmitmockTest);

module.exports = router;
