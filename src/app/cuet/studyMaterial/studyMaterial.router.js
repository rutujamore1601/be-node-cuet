
const router = require("express").Router();
const StudyMaterialController = require('./studyMaterial.controller');

// Create Operation - Create StudyMaterial
router.post('/createStudyMaterial', StudyMaterialController.createStudyMaterial);

// Read Operation - Get all StudyMaterial
router.get('/getAllStudyMaterial', StudyMaterialController.getAllStudyMaterial);

// Read Operation - Get all StudyMaterial by Id 
router.get("/getAllStudyMaterialById/:id", StudyMaterialController.getAllStudyMaterialById);

// Read Operation - Get a single StudyMaterial by Id
router.get('/getStudyMaterialById/:id', StudyMaterialController.getStudyMaterialById);

router.get('/getStudyMaterialByClassId/:classId/:medium', StudyMaterialController.getStudyMaterialByClassId);

// Update Operation - Update StudyMaterial
router.put('/updateStudyMaterial/:id', StudyMaterialController.updateStudyMaterial);

// Delete Operation - Delete StudyMaterial
router.delete('/deleteStudyMaterial/:id', StudyMaterialController.deleteStudyMaterial);

module.exports = router;
