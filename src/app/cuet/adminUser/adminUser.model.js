
const mongoose = require('mongoose');

const adminUserSchema = new mongoose.Schema({
  mobileNo: { type: Number, required: false },
  password:{type:String,required:false}
}, { timestamps: true });

module.exports = mongoose.model('adminUser', adminUserSchema);
