
const purchaseStudyMaterialModel = require('./purchaseStudyMaterial.model');
const purchaseStudyMeterialPaymemntModel = require('./../purchaseStudyMeterialPaymemnt/purchaseStudyMeterialPaymemnt.model');

  // Create Operation - Create purchaseStudyMaterial
  const createPurchaseStudyMaterial = (req, res) => {
    if (!req.body) {
      res.status(400).send({ message: "Content can not be empty!" });
      return;
    }
  
    const purchaseStudyMaterial = new purchaseStudyMaterialModel(req.body);
    purchaseStudyMaterial
      .save()
      .then((data) => {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "purchaseStudyMaterial created successfully!",
          success: true,
          statusCode: 200,
        });
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the purchaseStudyMaterial.",
        });
      });
  };

  // Read Operation - Get all purchaseStudyMaterial
  const getAllPurchaseStudyMaterial = (req, res) => {
    purchaseStudyMaterialModel.find()
  .sort({ classId: 1 })
  .then((data) => {
    data.sort((a, b) => parseInt(a.classId) - parseInt(b.classId));
  
        if (data.length > 0) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "purchaseStudyMaterial fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: [],
            dataCount: 0,
            message: "purchaseStudyMaterial not found for specified classIds!",
            success: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || "Some error occurred while Retrieve the purchaseStudyMaterial.",
        });
      });
  };
  

  // Read Operation - Get all purchaseStudyMaterial by Id 
  const getAllPurchaseStudyMaterialById  = (req, res) => {
    const id = req.params.id;
    const condition = { _id: id};

    purchaseStudyMaterialModel.find(condition)
      .then((data) => {
        if (data.length > 0) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "purchaseStudyMaterial fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: [],
            dataCount: data.length,
            message: "purchaseStudyMaterial not found!",
            success: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the purchaseStudyMaterial.",
        });
      });
  };

  // Read Operation - Get a single purchaseStudyMaterial by Id
  const getPurchaseStudyMaterialById = (req, res) => {
    const id = req.params.id;
    purchaseStudyMaterialModel.findById(id)
      .then((data) => {
        if (data) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "purchaseStudyMaterial fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: {},
            dataCount: 0,
            message: 'purchaseStudyMaterial not found with ID=' + id,
            status: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the purchaseStudyMaterial.",
        });
      });
  };

  // Read Operation - Get a single purchaseStudyMaterial by Id
  const getPurchaseStudyMaterialByClassId = (req, res) => {
    const classId = req.params.classId;
    const medium = req.params.medium;
    // const condition = {classId: classId, medium: medium};
    const userId = req.params.userId;
  
    purchaseStudyMaterialModel.findOne({classId,medium}).lean()
      .then((data) => {
      purchaseStudyMeterialPaymemntModel.findOne({$and: [
        {materialId:data._id},
        {userId:userId}
      ] }) .then((payInfo) => {
        if(payInfo){
          data.isPurchased = true;
        }else{
          data.isPurchased = false;
          delete data.questionBank;
          delete data.test;
          delete data.materialImage;
        }
        if (data) {
          res.status(200).send({
            data: data,
            dataCount: 1,
            message: "purchaseStudyMaterial fetched successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: null,
            dataCount: 0,
            message: 'purchaseStudyMaterial not found with classId=' + classId,
            success: false,
            statusCode: 200,
          });
        }
      })

      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || "Some error occurred while retrieving the purchaseStudyMaterial.",
          success: false,
          statusCode: 500,
        });
      });
  };
  
  

  // Update Operation - Update purchaseStudyMaterial
 const updatePurchaseStudyMaterial = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  purchaseStudyMaterialModel.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "purchaseStudyMaterial was updated successfully.",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message: 'Cannot update purchaseStudyMaterial with order ID=' + id + '. Maybe purchaseStudyMaterial was not found!',
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating purchaseStudyMaterial with ID=" + id,
      });
    });
};

// Delete Operation - Delete purchaseStudyMaterial
  const deletePurchaseStudyMaterial = (req, res) => {
    const id = req.params.id;
  
    purchaseStudyMaterialModel.findByIdAndDelete(id)
      .then((data) => {
        if (data) {
          res.status(200).send({
            message: "purchaseStudyMaterial was deleted successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(404).send({
            message: 'Cannot delete purchaseStudyMaterial with ID=' + id + '. Maybe purchaseStudyMaterial was not found!',
            success: false,
            statusCode: 404,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Tutorial.",
        });
      });
  };

module.exports = {
  createPurchaseStudyMaterial,
  getAllPurchaseStudyMaterial,
  getAllPurchaseStudyMaterialById,
  getPurchaseStudyMaterialById,
  updatePurchaseStudyMaterial,
  deletePurchaseStudyMaterial,
  getPurchaseStudyMaterialByClassId
};
