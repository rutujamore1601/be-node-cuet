
const router = require("express").Router();
const mockTestQuestionController = require('./mockTestQuestion.controller');

// Create Operation - Create mockTestQuestion
router.post('/createmockTestQuestion', mockTestQuestionController.createmockTestQuestion);

router.post('/bulkUpload', mockTestQuestionController.bulkUpload);

// router.post('/editOptionsByQuestionId', mockTestQuestionController.editOptionsByQuestionId);

// Read Operation - Get all mockTestQuestion
router.get('/getAllmockTestQuestion', mockTestQuestionController.getAllmockTestQuestion);

// Read Operation - Get all mockTestQuestion by Id 
router.get("/getAllmockTestQuestionById/:id", mockTestQuestionController.getAllmockTestQuestionById);

// Read Operation - Get a single mockTestQuestion by Id
router.get('/getmockTestQuestionById/:id', mockTestQuestionController.getmockTestQuestionById);

router.get('/getMockTestQuestionByTestId/:id', mockTestQuestionController.getMockTestQuestionByTestId);

// Update Operation - Update mockTestQuestion
router.put('/updatemockTestQuestion/:id', mockTestQuestionController.updatemockTestQuestion);

// Delete Operation - Delete mockTestQuestion
router.delete('/deletemockTestQuestion/:id', mockTestQuestionController.deletemockTestQuestion);

module.exports = router;
