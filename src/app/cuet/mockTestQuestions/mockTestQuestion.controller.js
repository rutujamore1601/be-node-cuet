const mockTestQuestionModel = require("./mockTestQuestion.model");
const mockTestModel = require("../mockTest/mockTest.model");

const bulkUpload = async (req, res) => {
  try {
    const jsonData = req.body;

    if (!Array.isArray(jsonData)) {
      throw new Error("Invalid JSON format. The JSON data must be an array.");
    }

    const bulkInsertData = jsonData.map(
      ({
        testId,
        question,
        optionValue1,
        isCorrect1,
        optionValue2,
        isCorrect2,
        optionValue3,
        isCorrect3,
        optionValue4,
        isCorrect4,
      }) => ({
        testId,
        question,
        optionList: [
          {
            optionValue: optionValue1,
            optionId: generateUniqueId(),
            isCorrect: isCorrect1,
          },
          {
            optionValue: optionValue2,
            optionId: generateUniqueId(),
            isCorrect: isCorrect2,
          },
          {
            optionValue: optionValue3,
            optionId: generateUniqueId(),
            isCorrect: isCorrect3,
          },
          {
            optionValue: optionValue4,
            optionId: generateUniqueId(),
            isCorrect: isCorrect4,
          },
        ],
      })
    );

    const data = await mockTestQuestionModel.insertMany(bulkInsertData);

    res.status(200).json({
      data: data,
      dataCount: data.length,
      message: "mockTestQuestion uploaded successfully!",
      success: true,
      statusCode: 200,
    });
  } catch (err) {
    res.status(500).json({
      message:
        err.message ||
        "Some error occurred while uploading the mockTestQuestion.",
      success: false,
      statusCode: 500,
    });
  }
};

// Create Operation - Create mockTestQuestion
// const createmockTestQuestion = (req, res) => {
//   if (!req.body) {
//     res.status(400).send({ message: "Content can not be empty!" });
//     return;
//   }

//   const options = req.body.optionList;

//   const optionList = options.map((o) => ({
//     optionValue: o.optionValue,
//     optionId: generateUniqueId(),
//     isCorrect: o.isCorrect,
//   }));

//   const mockTestQuestion = new mockTestQuestionModel({
//     ...req.body,
//     optionList,
//   });

//   mockTestQuestion
//     .save()
//     .then((data) => {
//       res.status(200).send({
//         data: data,
//         dataCount: 1,
//         message: "mockTestQuestion created successfully!",
//         success: true,
//         statusCode: 200,
//       });
//     })
//     .catch((err) => {
//       res.status(500).send({
//         message:
//           err.message ||
//           "Some error occurred while creating the mockTestQuestion.",
//       });
//     });
// };

const createmockTestQuestion = async (req, res) => {
  try {
    const { mockTestId, question } = req.body;

    const options = req.body.optionList;

    const optionList = options.map((o) => ({
      optionValue: o.optionValue,
      optionId: generateUniqueId(),
      isCorrect: o.isCorrect,
    }));

    const createQuestions = await mockTestQuestionModel.create({
      testId: mockTestId,
      question,
      optionList,
    });

    if (!createQuestions) {
      res.status(400).json("Error to create question..");
      return;
    }

    res.status(201).json("Question created successfully");
  } catch (error) {
    console.log("error", error);
    res.status(500).json("Internal Server Error");
  }
};

// Read Operation - Get all mockTestQuestion
// const getAllmockTestQuestion = (req, res) => {
//   mockTestQuestionModel
//     .find()
//     .populate({
//       path: "testId",
//     })
//     .then((data) => {
//       if (data.length > 0) {
//         res.status(200).send({
//           data: data,
//           dataCount: data.length,
//           message: "mockTestQuestion fetch successfully!",
//           success: true,
//           statusCode: 200,
//         });
//       } else {
//         res.status(200).send({
//           data: [],
//           dataCount: data.length,
//           message: "mockTestQuestion not found!",
//           success: false,
//           statusCode: 200,
//         });
//       }
//     })
//     .catch((err) => {
//       res.status(500).send({
//         message:
//           err.message ||
//           "Some error occurred while Retrieve the mockTestQuestion.",
//       });
//     });
// };

const getAllmockTestQuestion = async (req, res) => {
  try {
    const mockTests = await mockTestQuestionModel.find();
    // console.log("mockTests", mockTests);

    if (mockTests.length === 0) {
      res.status(404).json({ error: "Mock Test data not found." });
      return;
    }

    const result = await Promise.all(
      mockTests.map(async (test) => {
        const testDetails = await mockTestModel.findById(test.testId);
        if (!testDetails) {
          return {
            error: `Test details not found for subjectId: ${test.testId}`,
          };
        }

        return {
          test: {
            testId: testDetails._id,
            testName: testDetails.testName,
            testTimeDuration: testDetails.testTimeDuration,
          },
          question: test.question,
          optionList: test.optionList,
        };
      })
    );

    res.status(200).json(result);
  } catch (error) {
    console.error("Error:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

// Read Operation - Get all mockTestQuestion by Id
const getAllmockTestQuestionById = (req, res) => {
  const id = req.params.id;
  const condition = { _id: id };

  mockTestQuestionModel
    .find(condition)
    .populate({
      path: "testId",
    })
    .then((data) => {
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "mockTestQuestion fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: [],
          dataCount: data.length,
          message: "mockTestQuestion not found!",
          success: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while Retrieve the mockTestQuestion.",
      });
    });
};

// Read Operation - Get all mockTestQuestion by Id
const getmockTestQuestionById = (req, res) => {
  const testId = req.params.id;
  const condition = { testId: testId };

  mockTestQuestionModel
    .find(condition)
    .populate({
      path: "testId",
    })
    .then((data) => {
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "mockTestQuestion fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: [],
          dataCount: data.length,
          message: "mockTestQuestion not found!",
          success: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || getAllmockTestQuestion,
        // "Some error occurred while Retrieve the mockTestQuestion.",
      });
    });
};

// Read Operation - Get a single mockTestQuestion by Id
// const getmockTestQuestionById = (req, res) => {
//   const id = req.params.id;
//   mockTestQuestionModel.findById(id)
//   .populate({
//     path: "testId",
//   })
//     .then((data) => {
//       if (data) {
//         res.status(200).send({
//           data: data,
//           dataCount: data.length,
//           message: "mockTestQuestion fetch successfully!",
//           success: true,
//           statusCode: 200,
//         });
//       } else {
//         res.status(200).send({
//           data: {},
//           dataCount: 0,
//           message: 'mockTestQuestion not found with ID=' + id,
//           status: false,
//           statusCode: 200,
//         });
//       }
//     })
//     .catch((err) => {
//       res.status(500).send({
//         message:
//           err.message || "Some error occurred while Retrieve the mockTestQuestion.",
//       });
//     });
// };

// Read Operation - Get a single mockTest by Id
const getMockTestQuestionByTestId = (req, res) => {
  const testId = req.params.id;
  console.log("testId", testId);

  mockTestQuestionModel
    .find({ testId })
    .then((data) => {
      if (data) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "mockTestQuestion fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: {},
          dataCount: 0,
          message: "mockTestQuestion not found with classId=" + classId,
          status: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while Retrieve the StudyMaterial.",
      });
    });
};

// Update Operation - Update mockTestQuestion
const updatemockTestQuestion = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  mockTestQuestionModel
    .findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "mockTestQuestion was updated successfully.",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot update mockTestQuestion with order ID=" +
            id +
            ". Maybe mockTestQuestion was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating mockTestQuestion with ID=" + id,
      });
    });
};

// const editOptionsByQuestionId = async (req, res) => {
//   try {
//     const qId = req.body.questionId;
//     const options = req.body.options;
//     const optionList = [];

//     options.forEach((o) => {
//       const option_id = new mongoose.Types.ObjectId();
//       optionList.push({
//         optionValue: o.optionValue,
//         optionId: option_id,
//         isCorrect: o.isCorrect,
//       });
//     });

//     const question = await mockTestQuestionModel.findOneAndUpdate(
//       { _id: qId },
//       {
//         $set: {
//           optionList: optionList,
//           isActive: '1',
//         },
//       },
//       { new: true }
//     );

//     if (question) {
//       res.status(200).json({ message: 'Success' });
//     } else {
//       res.status(404).json({ error: 'Question not found' });
//     }
//   } catch (error) {
//     console.error(error);
//     res.status(500).json({ error: 'Internal server error' });
//   }
// };

// A simple function to generate a unique ID
function generateUniqueId() {
  return Date.now().toString(36) + Math.random().toString(36).substr(2);
}

// Delete Operation - Delete mockTestQuestion
const deletemockTestQuestion = (req, res) => {
  const id = req.params.id;

  mockTestQuestionModel
    .findByIdAndDelete(id)
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "mockTestQuestion was deleted successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot delete mockTestQuestion with ID=" +
            id +
            ". Maybe mockTestQuestion was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Tutorial.",
      });
    });
};

module.exports = {
  createmockTestQuestion,
  getAllmockTestQuestion,
  getAllmockTestQuestionById,
  getmockTestQuestionById,
  updatemockTestQuestion,
  deletemockTestQuestion,
  getMockTestQuestionByTestId,
  bulkUpload,
};
