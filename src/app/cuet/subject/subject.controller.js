
const subjectModel = require('./subject.model');

  // Create Operation - Create subject
  const createSubject = (req, res) => {
    if (!req.body) {
      res.status(400).send({ message: "Content can not be empty!" });
      return;
    }
  
    const subject = new subjectModel(req.body);
    subject
      .save()
      .then((data) => {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "subject created successfully!",
          success: true,
          statusCode: 200,
        });
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the subject.",
        });
      });
  };

  // Read Operation - Get all subject
  const getAllSubject = (req, res) => {
    subjectModel.find()
      .then((data) => {
        if (data.length > 0) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "subject fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: [],
            dataCount: data.length,
            message: "subject not found!",
            success: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the subject.",
        });
      });
  };

  // Read Operation - Get all subject by Id 
  const getAllSubjectById  = (req, res) => {
    const id = req.params.id;
    const condition = { _id: id};

    subjectModel.find(condition)
      .then((data) => {
        if (data.length > 0) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "subject fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: [],
            dataCount: data.length,
            message: "subject not found!",
            success: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the subject.",
        });
      });
  };

  // Read Operation - Get a single subject by Id
  const getSubjectById = (req, res) => {
    const id = req.params.id;
    subjectModel.findById(id)
      .then((data) => {
        if (data) {
          res.status(200).send({
            data: data,
            dataCount: data.length,
            message: "subject fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: {},
            dataCount: 0,
            message: 'subject not found with ID=' + id,
            status: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the subject.",
        });
      });
  };

  // Read Operation - Get a single subject by ClassId
  const getSubjectByClass = (req, res) => {
    const classId = req.params.classId;
    subjectModel.findOne({ classId: classId })
      .then((data) => {
        if (data) {
          res.status(200).send({
            data: data,
            dataCount: 1,
            message: "subject fetch successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(200).send({
            data: {},
            dataCount: 0,
            message: 'subject not found with ID=' + classId,
            status: false,
            statusCode: 200,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while Retrieve the subject.",
        });
      });
};

  // Update Operation - Update subject
  const updateSubject = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  subjectModel.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "subject was updated successfully.",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message: 'Cannot update subject with order ID=' + id + '. Maybe subject was not found!',
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating subject with ID=" + id,
      });
    });
  };

// Delete Operation - Delete subject
  const deleteSubject = (req, res) => {
    const id = req.params.id;
  
    subjectModel.findByIdAndDelete(id)
      .then((data) => {
        if (data) {
          res.status(200).send({
            message: "subject was deleted successfully!",
            success: true,
            statusCode: 200,
          });
        } else {
          res.status(404).send({
            message: 'Cannot delete subject with ID=' + id + '. Maybe subject was not found!',
            success: false,
            statusCode: 404,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Tutorial.",
        });
      });
  };

module.exports = {
  createSubject,
  getAllSubject,
  getAllSubjectById,
  getSubjectById,
  updateSubject,
  deleteSubject,
  getSubjectByClass
};
