
const mongoose = require('mongoose');

const subjectSchema = new mongoose.Schema({
  subjectName: { type: String, required: false },
  subjectImg: { type: String, required: false },
  streamId: { 
    type: mongoose.Schema.Types.ObjectId,
    ref: "stream",
    required: false, 
  },
}, { timestamps: true });

module.exports = mongoose.model('subject', subjectSchema);
