const addElerningModel = require("./addElerning.model");

// Create Operation - Create addElerning
const createAddElerning = (req, res) => {
  if (!req.body) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }

  const addElerning = new addElerningModel(req.body);
  addElerning
    .save()
    .then((data) => {
      res.status(200).send({
        data: data,
        dataCount: data.length,
        message: "addElerning created successfully!",
        success: true,
        statusCode: 200,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the addElerning.",
      });
    });
};

// Read Operation - Get all addElerning
const getAllAddElerning = (req, res) => {
  addElerningModel
    .find()
    .then((data) => {
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "addElerning fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: [],
          dataCount: data.length,
          message: "addElerning not found!",
          success: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while Retrieve the addElerning.",
      });
    });
};

// Read Operation - Get all addElerning by Id
const getAllAddElerningById = (req, res) => {
  const id = req.params.id;
  const condition = { _id: id };

  addElerningModel
    .find(condition)
    .then((data) => {
      if (data.length > 0) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "addElerning fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: [],
          dataCount: data.length,
          message: "addElerning not found!",
          success: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while Retrieve the addElerning.",
      });
    });
};

// Read Operation - Get a single addElerning by Id
const getAddElerningById = (req, res) => {
  const id = req.params.id;
  addElerningModel
    .findById(id)
    .populate({
      path: "streamId",
    })
    .then((data) => {
      if (data) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "addElerning fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: {},
          dataCount: 0,
          message: "addElerning not found with ID=" + id,
          status: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while Retrieve the addElerning.",
      });
    });
};

// Read Operation - Get a single addElerning by Id
const getYoutubeVideoByClassId = (req, res) => {
  const classId = req.params.classId;
  const medium = req.params.medium;
  const condition = { classId: classId, medium: medium };

  addElerningModel
    .find(condition)
    .then((data) => {
      if (data) {
        res.status(200).send({
          data: data,
          dataCount: data.length,
          message: "youTube video fetch successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(200).send({
          data: {},
          dataCount: 0,
          message: "youTube video not found with ID=" + id,
          status: false,
          statusCode: 200,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message ||
          "Some error occurred while Retrieve the youTube video.",
      });
    });
};

// Update Operation - Update addElerning
const updateAddElerning = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  addElerningModel
    .findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "addElerning was updated successfully.",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot update addElerning with order ID=" +
            id +
            ". Maybe addElerning was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating addElerning with ID=" + id,
      });
    });
};

// Delete Operation - Delete addElerning
const deleteAddElerning = (req, res) => {
  const id = req.params.id;

  addElerningModel
    .findByIdAndDelete(id)
    .then((data) => {
      if (data) {
        res.status(200).send({
          message: "addElerning was deleted successfully!",
          success: true,
          statusCode: 200,
        });
      } else {
        res.status(404).send({
          message:
            "Cannot delete addElerning with ID=" +
            id +
            ". Maybe addElerning was not found!",
          success: false,
          statusCode: 404,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Tutorial.",
      });
    });
};

module.exports = {
  createAddElerning,
  getAllAddElerning,
  getAllAddElerningById,
  getAddElerningById,
  updateAddElerning,
  deleteAddElerning,
  getYoutubeVideoByClassId,
};
