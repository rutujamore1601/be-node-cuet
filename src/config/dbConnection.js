const mongoose = require("mongoose");
const config = require("./config");
const dotenv = require("dotenv");
mongoose.set("strictQuery", false);

// const connectionString = `${config.db.dbUrl()}`;
const connectionString = process.env.connectionStrings;

mongoose
  .connect(connectionString, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
  })
  .then(() => {
    console.log(`### DB Connected to ${connectionString}`);
  })
  .catch((err) => console.log(err));

  //db change