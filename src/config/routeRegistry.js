const cuet = (app) => {
  // Add your middleware here if needed
  app.use("/studentRegistration", require("../app/cuet/studentRegistration/studentRegistration.router"));
  app.use("/uploadVideo", require("../app/cuet/uploadVideo/uploadVideo.router"));
  app.use("/uploadImage", require("../app/cuet/uploadImage/uploadImage.router"));
  app.use("/mockTestQuestions", require("../app/cuet/mockTestQuestions/mockTestQuestion.router"));
  app.use("/subject", require("../app/cuet/subject/subject.router"));
  app.use("/stream", require("../app/cuet/stream/stream.router"));
  app.use("/addElerning", require("../app/cuet/addElerning/addElerning.router"));
  app.use("/submitmockTest", require("../app/cuet/submitmockTest/submitmockTest.router"));
  app.use("/purchaseStudyMaterial", require("../app/cuet/purchaseStudyMaterial/purchaseStudyMaterial.router"));
  app.use("/purchaseStudyMeterialPaymemnt", require("../app/cuet/purchaseStudyMeterialPaymemnt/purchaseStudyMeterialPaymemnt.router"));
  app.use("/studyMaterial", require("../app/cuet/studyMaterial/studyMaterial.router"));
  app.use("/mockTest", require("../app/cuet/mockTest/mockTest.router"));
  app.use("/adminUser", require("../app/cuet/adminUser/adminUser.router"));
};

const routes = {
    cuet,
};

module.exports = routes[process.env.PROJ_NAME];
